Anima Christi
Absalon fili mi
O salutaris Hostia
Vexilla Regis
O Jesu fili David
In flagellis
Ecce tu pulchra es
Ave Christe immolate
Salve lux mundi
De profundis (Desprez)
A custodia
Ave Maria (Willaert)
Regina Coeli (Willaert)
Angelus ad pastores
Te Deum
Angeli Archangeli
Tollite jugum
Ave Maria (Palestrina)
Sicut cervus
Sitivit anima mea
Super flumina
Alma redemptoris
Regina Coeli (Palestrina)
Adoramus Te Christe
Jesu Rex
Tua Jesu dilectio
Jesu flos
Conditor
Jesu dulcis
O magnum
Ne timeas
Amicus (De Victoria)
Eram quasi agnus (De Victoria)
Tenebrae
Animam meam
Caligaverunt
Astiterunt reges
Popule meus
Quem dicunt
Estote fortes
O bone Jesu
Amicus (Ingegneri)
Eram quasi agnus (Ingegneri)
Stava a' pie'
Interna sete ardente
O cor soave
S'acceso
Cristo al morir
Nell'apparir (a 3)
Nell'apparir (a 4)
Accetta questo pane
Allein Gott
Aus meines
O caput cruentatum
Per Te Signore
Se Tu m'accogli
Se Tu m'accogli 2
Se Tu m'accogli 3
Se Tu m'accogli 4
Discendi