from music21 import *
from sys import exit
import os

def converting(url):
    if len(url)<4:
        return None
    else:
        return cambiapausa(pitches(converter.parse(url)))

def pitches(track):
    pitch=[]
    for p in track:
        pitchs=[]
        for n in p.flat.notes:
            pitchs.append(n.pitch.midi)
        pitch.append(pitchs)
    return pitch

def cambiapausa(track):
    if len(track)==1:
        track=track[0]
        for i in range(len(track)):
            if track[i]<=20:
                track[i]+=500
    else:
        for t in track:
            for i in range(len(t)):
                if t[i]<=20:
                    t[i]+=500
    return track

import numpy as np
from dtw import dtw
from collections import Counter
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from itertools import combinations

# Wrap pitch value in a class `Pitch`, `next` is a member containing
# the next pitch (of type `Pitch`) to be played in the voice leading sequence.
# It is `None` if it is the end of the sequence.
class Pitch:
  def __init__(self, value, offset):
    self.value = value
    self.offset = offset
    self.source_offset = offset
  def __repr__(self):
    return "(" + str(self.value) + "," + str(self.offset) + "," + str(self.source_offset) + ")"

def merge_max_multiset(res, from_multiset):
  for (e,n) in from_multiset.items():
    res[e] = max(res[e], n)

def map_source_to_target(sources, targets, pitches, target_idx, test):
  for (s, t) in zip(sources, targets):
    if test(s,t):
      source = pitches[s]
      target = pitches[t]
      target_idx[source.source_offset] = target.offset
      target.offset += 1
      source.source_offset += 1
  #print(target_idx)

def is_unison(s, t):
  return s == t

def is_not_unison(s, t):
  return not is_unison(s, t)

def zeropause(M):
  res=np.zeros((M.shape[0],M.shape[1]))
  for s in range(M.shape[0]):
    for t in range(M.shape[1]):
      if M[s][t]!=-1:
        res[s][t]=M[s][t]
  return res

def creacross(sources,targets,vl1,vl2):
    lead=zip(sources,targets,range(len(sources)))
    v1=(-1,(0,0))
    v2=(-1,(0,0))
    diff=1
    for x in lead:
      if x[:-1]==vl1 and v2[0]!=x[-1]:
        v1=(x[-1],vl1)
    for x in lead:
      if x[:-1]==vl2 and v1[0]!=x[-1]:
        v2=(x[-1],vl2)
    if v1!=-1 and v2!=-1:
      v12=(v1,v2)
      v12=sorted(v12)
      diff_s=np.sign(v12[0][1][0]-v12[1][1][0])
      diff_t=np.sign(v12[0][1][1]-v12[1][1][1])
      if diff_s==0:
        diff=diff_t
      if diff_t==0:
        diff=diff_s
    prob_cross=[(v12[0][0],v12[1][0]),diff]
    return prob_cross

prob_cross=[]
unipause=0
def partial_permut(sources, targets,prob_cross,unipause):
  multi_sources = Counter(sources)
  multi_targets = Counter(targets)

  #print(multi_sources)

  v = Counter()
  merge_max_multiset(v, multi_sources)
  merge_max_multiset(v, multi_targets)

  counted_pitches = sorted(v.items())
##  print(counted_pitches)

  pitches = {}
  counting_sum = 0
  for p, n in counted_pitches:
    pitches[p] = Pitch(p, counting_sum)
    counting_sum += n
##  print(pitches)

  v = sorted(v.elements())
##  print(v)

  target_idx = {}
  map_source_to_target(sources, targets, pitches, target_idx, is_unison)
  map_source_to_target(sources, targets, pitches, target_idx, is_not_unison)
##  print(target_idx)

  # print sources, targets,target_idx.items(),v
  # Create the matrix `M` where `M[i][j] == 1` if the pitch at index `i` leads to the pitch at index `j`.
  dim = len(v)
  paused = []
  M = np.zeros((dim, dim))
  for (s, t) in target_idx.items():
    if v[s] < 500 and v[t] < 500:
        M[s][t] = 1
    else:
        M[s][t] = -1
        paused.append([s,t])

##  print M

  if paused:
    Minor = zeropause(M)
  else:
        Minor = M
##  print Minor

  c = [np.count_nonzero(np.triu(Minor,1)), np.count_nonzero(np.tril(Minor,-1)), np.count_nonzero(np.diag(Minor)), 0, 0, 0]
  cross = 0
  unison = unipause
  unipause=0

  non_zero = np.nonzero(Minor)
  couple = zip(non_zero[0] , non_zero[1])

##  print couple
  for ((i,j),(k,l)) in combinations(couple,2):
##    print (i,j),(k,l)
    if v[i]!=v[k] and v[j]!=v[l]:
      if (i<j and k>l) and (i<k and j>l) or (i<=j and k>l) and (i<k and j>l) or (i<j and k>=l) and (i<k and j>l):
##        print (i,j),(k,l)
##        print "Cross1"
        cross+=1
      elif (i<k and j>l):
##        print (i,j),(k,l)
##        print "Cross2"
        cross+=1
    else:
##      print (i,j),(k,l)
      if v[i]!=v[k] and v[j]==v[l]:
##        print "Arrivo in unisono"
        prob_cross.append(creacross(sources,targets,(v[i],v[j]),(v[k],v[l])))
        unison+=1
##          print "prob_cross",prob_cross
      elif v[i]==v[k] and v[j]==v[l]:
##        print "Rimasti all'unisono"
        unison+=1
        prob_cross2=creacross(sources,targets,(v[i],v[j]),(v[k],v[l]))
        if prob_cross==[]:
##          print "Siamo all'inizio"
          prob_cross.append(prob_cross2)
        else:
          voci=[]
          for x in prob_cross:
            voci.append(x[0])
##              print "voci", voci
          if prob_cross2[0] not in voci:
##            print "Siamo all'inizio"
            prob_cross.append(prob_cross2)
      elif v[i]==v[k] and v[j]!=v[l]:
        prob_cross2=creacross(sources,targets,(v[i],v[j]),(v[k],v[l]))
##        print "prob_cross2",prob_cross2
        i=0
        voce=0
        while i<len(prob_cross):
          if prob_cross[i][0]==prob_cross2[0]:
##            print "uo!"
            if prob_cross[i][1]==-prob_cross2[1]:
              voce=prob_cross2[0]
##              print "Crossing da unisono!"
              cross+=1
##              print "ti cancello zio"
##            elif
            del prob_cross[i]
          else:
            i+=1
  i=0
  while i<len(prob_cross):
    if targets[prob_cross[i][0][0]]>500 or targets[prob_cross[i][0][1]]>500:
##      print "Da unisono a pausa"
      del prob_cross[i]
    else:
      i+=1
  for i in range(len(sources)):
    for j in range(i+1,len(sources)):
      if sources[i]>500 or sources[j]>500:
        if targets[i]==targets[j]:
##          print "Da pausa a unisono"
          prob_cross.append([(i,j),1])
          unipause+=1

##  print "prob_cross finale", prob_cross
  c[3] = cross
  c[4] = unison
  c[5] = len(paused)

##  return M, Minor, prob_cross, c,v, couple,paused,unipause
  return c, prob_cross, unipause

def convertistr(mat):
    nocapo=[]
    splittato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    return splittato

def convertifloat(mat):
    nocapo=[]
    splittato=[]
    floatato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    for x in splittato:
        for i in range(len(x)):
            x[i]=float(x[i])
        floatato.append(x)
    return floatato

Bdoc=open(os.path.join('index', '00MIDIcomp.txt'))
Btot=convertistr(Bdoc.readlines())
Bdoc.close()

from norms import *
##p=2
p=int(raw_input('Che norma vuoi? (0=infinito) '))
print(' ')
norma=norme[p-1]
print norma

B=Btot

for x in B:
    B1_inp=os.path.join('complete',x)

    B1=[]
    if len(B1_inp)!=1:
        for x in B1_inp:
            B1.append(converting(x))
    else:
        B1=converting(B1_inp[0])

    for i in range(len(B1)):
        for j in range(len(B1)):
            if not (len(B1[i])==len(B1[j])):
                print "Una delle voci non ha lo stesso numero di note delle altre"
                exit("Stop")

    array1 = []
    ##print 'BRANO 1'
    for i in range(len(B1[0])-1):
        C1=[]
        C2=[]
        for j in range(len(B1)):
            C1.append(B1[j][i])
            C2.append(B1[j][i+1])
        c, prob_cross, unipause = partial_permut(C1, C2,prob_cross,unipause)
        array1.append(c)

    for y in B:
        B2_inp=os.path.join('complete',y)

        B2=[]
        if len(B2_inp)!=1:
            for x in B2_inp:
                B2.append(converting(x))
        else:
            B2=converting(B2_inp[0])

        for i in range(len(B2)):
            for j in range(len(B2)):
                if not (len(B2[i])==len(B2[j])):
                    print "Una delle voci non ha lo stesso numero di note delle altre"
                    exit()

        array2 = []
        for i in range(len(B2[0])-1):
            C1=[]
            C2=[]
            for j in range(len(B2)):
                C1.append(B2[j][i])
                C2.append(B2[j][i+1])
            c, prob_cross, unipause = partial_permut(C1, C2,prob_cross,unipause)
            array2.append(c)

        dist,cost,acc,path = dtw(np.asarray(array1),np.asarray(array2),norma)
##        print B1_inp[0][9:-4].upper(), B2_inp[0][9:-4].upper(), dist
        print dist

##        plt.imshow(cost.T, origin='lower',  cmap='bone', interpolation='bicubic')
##        plt.plot(path[0], path[1], 'w')
##        plt.xlim((-0.5, cost.shape[0]-0.5))
##        plt.ylim((-0.5, cost.shape[1]-0.5))
##        plt.show()

##        print B1_inp[0][:-5].upper(), B2_inp[0][:-5].upper()
##        for x in norme:
##            dist,cost,acc,path = dtw(array1,array2,x)
##            print str(x)[10:15], dist

##l=0
##while l<len(B)-1:
##    B1_inp=B[l]
##    del(B[l])
##
##    B1=[]
##    if len(B1_inp)!=1:
##        for x in B1_inp:
##            B1.append(converting(x))
##    else:
##        B1=converting(B1_inp[0])
##
##    for i in range(len(B1)):
##        for j in range(len(B1)):
##            if not (len(B1[i])==len(B1[j])):
##                print "Una delle voci non ha lo stesso numero di note delle altre"
##                exit("Stop")
##
##    array1 = []
##    ##print 'BRANO 1'
##    for i in range(len(B1[0])-1):
##        C1=[]
##        C2=[]
##        for j in range(len(B1)):
##            C1.append(B1[j][i])
##            C2.append(B1[j][i+1])
##        c, prob_cross, unipause = partial_permut(C1, C2,prob_cross,unipause)
##        array1.append(c)
##
##    for y in B:
##        B2_inp=y
##
##        B2=[]
##        if len(B2_inp)!=1:
##            for x in B2_inp:
##                B2.append(converting(x))
##        else:
##            B2=converting(B2_inp[0])
##
##        for i in range(len(B2)):
##            for j in range(len(B2)):
##                if not (len(B2[i])==len(B2[j])):
##                    print "Una delle voci non ha lo stesso numero di note delle altre"
##                    exit()
##
##        array2 = []
##        for i in range(len(B2[0])-1):
##            C1=[]
##            C2=[]
##            for j in range(len(B2)):
##                C1.append(B2[j][i])
##                C2.append(B2[j][i+1])
##            c, prob_cross, unipause = partial_permut(C1, C2,prob_cross,unipause)
##            array2.append(c)
##
##        dist,cost,acc,path = dtw(array1,array2,norma)
##        print B1_inp[0][9:-4].upper(), B2_inp[0][9:-4].upper(), dist
##
####        plt.imshow(cost.T, origin='lower',  cmap='bone', interpolation='bicubic')
####        plt.plot(path[0], path[1], 'w')
####        plt.xlim((-0.5, cost.shape[0]-0.5))
####        plt.ylim((-0.5, cost.shape[1]-0.5))
####        plt.show()
##
####        print B1_inp[0][:-5].upper(), B2_inp[0][:-5].upper()
####        for x in norme:
####            dist,cost,acc,path = dtw(array1,array2,x)
####            print str(x)[10:15], dist
