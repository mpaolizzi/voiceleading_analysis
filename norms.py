import numpy as np
from scipy.spatial.distance import cdist
from scipy.spatial.distance import pdist

def norm1(x,y):
    return pdist((x,y),'minkowski',p=1)

def norm2(x,y):
    return pdist((x,y),'euclidean')

def norm3(x,y):
    return pdist((x,y),'minkowski',p=3)

def norm4(x,y):
    return pdist((x,y),'minkowski',p=4)

def norm5(x,y):
    return pdist((x,y),'minkowski',p=5)

def norm6(x,y):
    return pdist((x,y),'minkowski',p=6)

def norm7(x,y):
    return pdist((x,y),'minkowski',p=7)

def norm8(x,y):
    return pdist((x,y),'minkowski',p=8)

def norm9(x,y):
    return pdist((x,y),'minkowski',p=9)

def norm10(x,y):
    return pdist((x,y),'minkowski',p=10)

def infty(x,y):
    return pdist((x,y),'chebyshev')


norme=[norm1,norm2,norm3,norm4,norm5,
       norm6,norm7,norm8,norm9,norm10,
       infty]
