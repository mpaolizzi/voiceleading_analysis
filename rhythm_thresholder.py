from music21 import converter, duration
import numpy as np
from collections import Counter
from string import digits
import os

## input: MIDI generato con Finale2012 senza Human Playback, ogni voce su canale distinto

def conv_length(midi):
    durate=[]
    for p in midi:
        d=[]
        for n in p.flat.notes:
            d.append(n.quarterLength)
        durate.append(d)
    durcomp=[]
    for i in durate:
        for j in i:
            durcomp.append(j)
    return durate, durcomp

dura=range(-6,3)
for i in range(len(dura)):
    dura[i]=2.**dura[i]

##"Scompone" i tempi composti inferiori alla breve, e unisce i valori degli elementi
##trovati ai corrispettivi elementi semplici.
def riducicomposti(dic):
    notescomp={}
    for x in dic:
        notescomp[x]=dic[x]
    
    for k in notescomp.keys():
##        print "k",k
        (frac, partint)=np.modf(k)
        if k not in dura and k<8:
            if frac!=0 and partint!=0:
##                print "frazioni ok"
##                print (frac, partint)
                notescomp[frac]+=notescomp[k]
                notescomp[partint]+=notescomp[k]
##                print notescomp[frac],notescomp[partint]
                del notescomp[k]
            else:
                for y in dura:
                    if k-y in dura:
##                        print "Ci sono con", y, k-y
                        notescomp[k-y]+=notescomp[k]
                        notescomp[y]+=notescomp[k]
##                        print notescomp[y],notescomp[k-y]
                        del notescomp[k]
                        break
##            print notescomp.keys()
    return notescomp

def pesanotetempo(dic):
    notepes={}
    for x in dic:
        notepes[x]=x*dic[x]
    base=sum(notepes.values())
    for x in notepes:
        notepes[x]=(float(notepes[x])/base)*100

    return notepes

def verifica(inp):
    char=[]
    for x in inp:
        char.append(x not in digits)
    rif=[True]*len(char)
    return char==rif

def convertistr(mat):
    nocapo=[]
    splittato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    return splittato

##Peso in base al tempo
##I valori sotto l'1% li trascuro
##Valori dalla croma in giu', fino al 4% riduzione locale
##sopra il 4, durata main
def valutapeso(midi):
    durate, durcomp=conv_length(converter.parse(x))
    
    note=dict(Counter(durcomp))
    notescomp=riducicomposti(note)
    
    notepes=pesanotetempo(notescomp)
    
    (low,med,high)=(0,1,4)
    trascura=[]
    locale=[]
    totale=0
    for (k, val) in sorted(notepes.items()):
        if low<=val<=med and k<=4:
            trascura.append((k,val))
        elif med<val<=high:
            locale.append((k,val))
        elif val>high:
            totale=(k,val)
            break
    
    return durate, note, notepes, trascura, locale, totale

doc=open(os.path.join('index','00MIDIorig.txt'))

lista=convertistr(doc.readlines())
for i in range(len(lista)):
    if len(lista[i])==1:
        lista[i]=lista[i][0]
doc.close()

##x=lista[8]

for i in range(len(lista)):
    print "Digitare", i+1, "per il MIDI", lista[i][9:].upper()

print(' ')
j=raw_input('Quale midi si desidera? T per tutto... ')
print(' ')

if verifica(j):
##    tot=[]
##    nomi=[]
    for x in lista:
        durate, note, notepes, trascura, locale, totale = valutapeso(os.path.join('midiorig',x))
##        tot.append(notepes)
##        nome=x[9:-8].upper()
##        nomi.append(nome)
        print(' ')
        print x[9:-8].upper(), note
        print(' ')
##        print notepes

        print "Suddivisione principale:"
        print totale[0], "avente percentuale", np.round(totale[1],decimals=2)

        if len(locale)!=0:
            print (' ')
            print "Occorre suddividere localmente i valori corrispondenti a:"
            for i in locale:
                print i[0], "con percentuale", np.round(i[1],decimals=2)

        if len(trascura)!=0:
            print(' ')
            print "E' possibile trascurare:"
            for i in trascura:
                print i[0], "avente percentuale", np.round(i[1],decimals=2)

else:
    x=lista[int(j)-1]

    durate, note, notepes, trascura, locale, totale = valutapeso(os.path.join('midiorig',x))
    
    print(' ')
    print x[9:-8].upper(), note
    print (' ')

    print "Suddivisione principale:"
    print totale[0], "avente percentuale", np.round(totale[1],decimals=2)

    if len(locale)!=0:
        print (' ')
        print "Occorre suddividere localmente i valori corrispondenti a:"
        for i in locale:
            print i[0], "con percentuale", np.round(i[1],decimals=2)

    if len(trascura)!=0:
        print (' ')
        print "E' possibile trascurare:"
        for i in trascura:
            print i[0], "avente percentuale", np.round(i[1],decimals=2)
