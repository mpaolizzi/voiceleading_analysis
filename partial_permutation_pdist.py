from music21 import *
from sys import exit
import os
##from music21 import converter, pitch, volume

def converting(url):
    if len(url)<4:
        return None
    else:
        return cambiapausa(pitches(converter.parse(url)))

def pitches(track):
    pitch=[]
    for p in track:
        pitchs=[]
        for n in p.flat.notes:
            pitchs.append(n.pitch.midi)
        pitch.append(pitchs)
    return pitch

def cambiapausa(track):
    if len(track)==1:
        track=track[0]
        for i in range(len(track)):
            if track[i]<=20:
                track[i]+=500
    else:
        for t in track:
            for i in range(len(t)):
                if t[i]<=20:
                    t[i]+=500
    return track


def convertistr(mat):
    nocapo=[]
    splittato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    return splittato

def convertifloat(mat):
    nocapo=[]
    splittato=[]
    floatato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    for x in splittato:
        for i in range(len(x)):
            x[i]=float(x[i])
        floatato.append(x)
    return floatato

import numpy as np
from dtw import dtw
from collections import Counter
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from itertools import combinations

# Wrap pitch value in a class `Pitch`, `next` is a member containing
# the next pitch (of type `Pitch`) to be played in the voice leading sequence.
# It is `None` if it is the end of the sequence.
class Pitch:
  def __init__(self, value, offset):
    self.value = value
    self.offset = offset
    self.source_offset = offset
  def __repr__(self):
    return "(" + str(self.value) + "," + str(self.offset) + "," + str(self.source_offset) + ")"

def merge_max_multiset(res, from_multiset):
  for (e,n) in from_multiset.items():
    res[e] = max(res[e], n)

def map_source_to_target(sources, targets, pitches, target_idx, test):
  for (s, t) in zip(sources, targets):
    if test(s,t):
      source = pitches[s]
      target = pitches[t]
      target_idx[source.source_offset] = target.offset
      target.offset += 1
      source.source_offset += 1
  #print(target_idx)

def is_unison(s, t):
  return s == t

def is_not_unison(s, t):
  return not is_unison(s, t)

def zeropause(M):
  res=np.zeros((M.shape[0],M.shape[1]))
  for s in range(M.shape[0]):
    for t in range(M.shape[1]):
      if M[s][t]!=-1:
        res[s][t]=M[s][t]
  return res

def creacross(sources,targets,vl1,vl2):
    lead=zip(sources,targets,range(len(sources)))
    v1=(-1,(0,0))
    v2=(-1,(0,0))
    diff=1
    for x in lead:
      if x[:-1]==vl1 and v2[0]!=x[-1]:
        v1=(x[-1],vl1)
    for x in lead:
      if x[:-1]==vl2 and v1[0]!=x[-1]:
        v2=(x[-1],vl2)
    if v1!=-1 and v2!=-1:
      v12=(v1,v2)
      v12=sorted(v12)
      diff_s=np.sign(v12[0][1][0]-v12[1][1][0])
      diff_t=np.sign(v12[0][1][1]-v12[1][1][1])
      if diff_s==0:
        diff=diff_t
      if diff_t==0:
        diff=diff_s
    prob_cross=[(v12[0][0],v12[1][0]),diff]
    return prob_cross

prob_cross=[]
unipause=0
def partial_permut(sources, targets,prob_cross,unipause):
  multi_sources = Counter(sources)
  multi_targets = Counter(targets)

  #print(multi_sources)

  v = Counter()
  merge_max_multiset(v, multi_sources)
  merge_max_multiset(v, multi_targets)

  counted_pitches = sorted(v.items())
##  print(counted_pitches)

  pitches = {}
  counting_sum = 0
  for p, n in counted_pitches:
    pitches[p] = Pitch(p, counting_sum)
    counting_sum += n
##  print(pitches)

  v = sorted(v.elements())
##  print(v)

  target_idx = {}
  map_source_to_target(sources, targets, pitches, target_idx, is_unison)
  map_source_to_target(sources, targets, pitches, target_idx, is_not_unison)
##  print(target_idx)

  # print sources, targets,target_idx.items(),v
  # Create the matrix `M` where `M[i][j] == 1` if the pitch at index `i` leads to the pitch at index `j`.
  dim = len(v)
  paused = []
  M = np.zeros((dim, dim))
  for (s, t) in target_idx.items():
    if v[s] < 500 and v[t] < 500:
        M[s][t] = 1
    else:
        M[s][t] = -1
        paused.append([s,t])

##  print M
  if paused:
    Minor = zeropause(M)
  else:
        Minor = M
##  print Minor

  c = [np.count_nonzero(np.triu(Minor,1)), np.count_nonzero(np.tril(Minor,-1)), np.count_nonzero(np.diag(Minor)), 0, 0, 0]
  cross = 0
  unison = unipause
  unipause=0

  non_zero = np.nonzero(Minor)
  couple = zip(non_zero[0] , non_zero[1])

##  print couple
  for ((i,j),(k,l)) in combinations(couple,2):
##    print (i,j),(k,l)
    if v[i]!=v[k] and v[j]!=v[l]:
      if (i<j and k>l) and (i<k and j>l) or (i<=j and k>l) and (i<k and j>l) or (i<j and k>=l) and (i<k and j>l):
##        print (i,j),(k,l)
        print "Cross1"
        cross+=1
      elif (i<k and j>l):
##        print (i,j),(k,l)
        print "Cross2"
        cross+=1
    else:
##      print (i,j),(k,l)
      if v[i]!=v[k] and v[j]==v[l]:
        print "Arrivo in unisono"
        prob_cross.append(creacross(sources,targets,(v[i],v[j]),(v[k],v[l])))
        unison+=1
##          print "prob_cross",prob_cross
      elif v[i]==v[k] and v[j]==v[l]:
        print "Rimasti all'unisono"
        unison+=1
        prob_cross2=creacross(sources,targets,(v[i],v[j]),(v[k],v[l]))
        if prob_cross==[]:
          print "Siamo all'inizio"
          prob_cross.append(prob_cross2)
        else:
          voci=[]
          for x in prob_cross:
            voci.append(x[0])
##              print "voci", voci
          if prob_cross2[0] not in voci:
            print "Siamo all'inizio"
            prob_cross.append(prob_cross2)
      elif v[i]==v[k] and v[j]!=v[l]:
        prob_cross2=creacross(sources,targets,(v[i],v[j]),(v[k],v[l]))
##        print "prob_cross2",prob_cross2
        i=0
        voce=0
        while i<len(prob_cross):
          if prob_cross[i][0]==prob_cross2[0]:
##            print "uo!"
            if prob_cross[i][1]==-prob_cross2[1]:
              voce=prob_cross2[0]
              print "Crossing da unisono!"
              cross+=1
##              print "ti cancello zio"
##            elif
            del prob_cross[i]
          else:
            i+=1
  i=0
  while i<len(prob_cross):
    if targets[prob_cross[i][0][0]]>500 or targets[prob_cross[i][0][1]]>500:
      print "Da unisono a pausa"
      del prob_cross[i]
    else:
      i+=1
  for i in range(len(sources)):
    for j in range(i+1,len(sources)):
      if sources[i]>500 or sources[j]>500:
        if targets[i]==targets[j]:
          print "Da pausa a unisono"
          prob_cross.append([(i,j),1])
          unipause+=1

##  print "prob_cross finale", prob_cross
  c[3] = cross
  c[4] = unison
  c[5] = len(paused)

##  return M, Minor, prob_cross, c,v, couple,paused,unipause
  return c, prob_cross,unipause

label=open(os.path.join('index', '0LABELSanni.txt'))
lab=convertistr(label.readlines())
for i in range(len(lab)):
    lab[i]=lab[i][0]

Bdoc=open(os.path.join('index', '00MIDIcomp.txt'))
Btot=convertistr(Bdoc.readlines())

Bdoc.close()
label.close()

k=2
##k=int(raw_input('Quanti brani vuoi analizzare? 1 o 2? '))
print(' ')

for i in range(len(lab)):
    print 'Digitare', i+1, 'per', lab[i]
print(' ')
test1=int(raw_input('Quale brano vuoi analizzare? '))
print(' ')
##test1=4
B1_inp=os.path.join('complete', Btot[test1-1])

B1=[]
if len(B1_inp)!=1:
    for x in B1_inp:
        B1.append(converting(x))
else:
    B1=converting(B1_inp[0])


for i in range(len(B1)):
    for j in range(len(B1)):
        if not (len(B1[i])==len(B1[j])):
            print "Una delle voci non ha lo stesso numero di note delle altre"
            exit("Stop")

if k==2:
    test2=int(raw_input('Con che brano lo vuoi confrontare? '))
    print(' ')
##    test2=17
    B2_inp=os.path.join('complete', Btot[test2-1])

    B2=[]
    if len(B2_inp)!=1:
        for x in B2_inp:
            B2.append(converting(x))
    else:
        B2=converting(B2_inp[0])

    for i in range(len(B2)):
        for j in range(len(B2)):
            if not (len(B2[i])==len(B2[j])):
                print "Una delle voci non ha lo stesso numero di note delle altre"
                exit()

array1 = []
print 'BRANO 1'
for i in range(len(B1[0])-1):
    C1=[]
    C2=[]
    for j in range(len(B1)):
        C1.append(B1[j][i])
        C2.append(B1[j][i+1])
##    print C1,C2
    c, prob_cross,unipause=partial_permut(C1,C2,prob_cross,unipause)
##    print(c)
    array1.append(c)
print 'ARRAY1', array1

if k==2:
    print 'BRANO 2'
    array2 = []
    for i in range(len(B2[0])-1):
        C1=[]
        C2=[]
        for j in range(len(B2)):
            C1.append(B2[j][i])
            C2.append(B2[j][i+1])
##        print C1,C2
        c, prob_cross,unipause=partial_permut(C1,C2,prob_cross,unipause)
##        print(c)
        array2.append(c)
    print 'ARRAY2',array2

# Compute paradigmatic complexity vectors and their multiplicity, print them as a multiset, compute multiplicity for each vector and give a 3d scatter of these arrays as
# cloud of massive points.
print 'BRANO 1'
uniq_complex_groups1 = [list(t) for t in set(map(tuple, array1))]
multiplicity1 = []
for p in uniq_complex_groups1:
    mult = array1.count(p)
    multiplicity1.append(mult)
print multiplicity1
print zip(uniq_complex_groups1, multiplicity1)

print uniq_complex_groups1

if k==2:
    print 'BRANO 2'
    uniq_complex_groups2 = [list(t) for t in set(map(tuple, array2))]
    multiplicity2 = []
    for p in uniq_complex_groups2:
        mult = array2.count(p)
        multiplicity2.append(mult)
    print multiplicity2
    print zip(uniq_complex_groups2, multiplicity2)

    print uniq_complex_groups2


##==============================================================================

##x11 = []
##y11 = []
##z11 = []
##for el in uniq_complex_groups1:
##     x11 += [el[0]]
##     y11 += [el[1]]
##     z11 += [el[2]]
###print x11,y11,z11
##fig11 = plt.figure()
##ax11 = fig11.add_subplot(111, projection='3d')
##
##s = [np.divide(1000*m, len(B1[0])) for m in multiplicity1]
##
##ax11.scatter(x11, y11, z11, c='r', marker='o',s=s)
##
##ax11.set_xlabel('#voices moving upward')
##ax11.set_ylabel('#voices moving downward')
##ax11.set_zlabel('#fixed voices')
##
##
##x12 = []
##y12 = []
##z12 = []
##for el in uniq_complex_groups1:
##     x12 += [el[0]]
##     y12 += [el[1]]
##     z12 += [el[3]]
##fig12 = plt.figure()
##ax12 = fig12.add_subplot(111, projection='3d')
##
##s = [np.divide(1000*m, len(B1[0])) for m in multiplicity1]
##
##ax12.scatter(x12, y12, z12, c='r', marker='o',s=s)
##
##ax12.set_xlabel('#voices moving upward')
##ax12.set_ylabel('#voices moving downward')
##ax12.set_zlabel('#voice crossing')
##
##x13 = []
##y13 = []
##z13 = []
##for el in uniq_complex_groups1:
##     x13 += [el[0]]
##     y13 += [el[1]]
##     z13 += [el[4]]
##fig13 = plt.figure()
##ax13 = fig13.add_subplot(111, projection='3d')
##
##s = [np.divide(1000*m, len(B1[0])) for m in multiplicity1]
##
##ax13.scatter(x13, y13, z13, c='r', marker='o',s=s)
##
##ax13.set_xlabel('#voices moving upward')
##ax13.set_ylabel('#voices moving downward')
##ax13.set_zlabel('#unison')
##
##x14 = []
##y14 = []
##z14 = []
##for el in uniq_complex_groups1:
##     x14 += [el[0]]
##     y14 += [el[1]]
##     z14 += [el[5]]
##fig14 = plt.figure()
##ax14 = fig14.add_subplot(111, projection='3d')
##
##s = [np.divide(1000*m, len(B1[0])) for m in multiplicity1]
##
##ax14.scatter(x14, y14, z14, c='r', marker='o',s=s)
##
##ax14.set_xlabel('#voices moving upward')
##ax14.set_ylabel('#voices moving downward')
##ax14.set_zlabel('#rests')
##
##plt.show()
##
##if k==2:
##    x21 = []
##    y21 = []
##    z21 = []
##    for el in uniq_complex_groups2:
##        x21 += [el[0]]
##        y21 += [el[1]]
##        z21 += [el[2]]
##    fig21 = plt.figure()
##    ax21 = fig21.add_subplot(111, projection='3d')
##
##    s = [np.divide(1000*m, len(B2[0])) for m in multiplicity2]
##
##    ax21.scatter(x21, y21, z21, c='r', marker='o',s=s)
##
##    ax21.set_xlabel('#voices moving upward')
##    ax21.set_ylabel('#voices moving downward')
##    ax21.set_zlabel('#fixed voices')
##
##    x22 = []
##    y22 = []
##    z22 = []
##
##    for el in uniq_complex_groups2:
##        x22 += [el[0]]
##        y22 += [el[1]]
##        z22 += [el[3]]
##    fig22 = plt.figure()
##    ax22 = fig22.add_subplot(111, projection='3d')
##
##    s = [np.divide(1000*m, len(B2[0])) for m in multiplicity2]
##
##    ax22.scatter(x22, y22, z22, c='r', marker='o',s=s)
##
##    ax22.set_xlabel('#voices moving upward')
##    ax22.set_ylabel('#voices moving downward')
##    ax22.set_zlabel('#voice crossing')
##
##    x23 = []
##    y23 = []
##    z23 = []
##
##    for el in uniq_complex_groups2:
##        x23 += [el[0]]
##        y23 += [el[1]]
##        z23 += [el[4]]
##    fig23 = plt.figure()
##    ax23 = fig23.add_subplot(111, projection='3d')
##
##    s = [np.divide(1000*m, len(B2[0])) for m in multiplicity2]
##
##    ax23.scatter(x23, y23, z23, c='r', marker='o',s=s)
##
##    ax23.set_xlabel('#voices moving upward')
##    ax23.set_ylabel('#voices moving downward')
##    ax23.set_zlabel('#unison')
##
##    x24 = []
##    y24 = []
##    z24 = []
##
##    for el in uniq_complex_groups2:
##        x24 += [el[0]]
##        y24 += [el[1]]
##        z24 += [el[5]]
##    fig24 = plt.figure()
##    ax24 = fig24.add_subplot(111, projection='3d')
##
##    s = [np.divide(1000*m, len(B2[0])) for m in multiplicity2]
##
##    ax24.scatter(x24, y24, z24, c='r', marker='o',s=s)
##
##    ax24.set_xlabel('#voices moving upward')
##    ax24.set_ylabel('#voices moving downward')
##    ax24.set_zlabel('#rests')
##
##    plt.show

##==============================================================================
from norms import *

if k==2:
    print(' ')
    print('Che norma vuoi utilizzare per la DTW?')
    p=int(raw_input('(0 per infinito)... '))
    norma=norme[p-1]
    dist,cost,acc,path = dtw(np.asarray(array1),np.asarray(array2),norma)
    print dist

    plt.imshow(cost.T, origin='lower',  cmap='bone', interpolation='bicubic')
    plt.plot(path[0], path[1], 'w')
    plt.xlim((-0.5, cost.shape[0]-0.5))
    plt.ylim((-0.5, cost.shape[1]-0.5))
    plt.show()
