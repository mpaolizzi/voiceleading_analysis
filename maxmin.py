import numpy as np
from gerarchia import *
from itertools import combinations

def convertistr(mat):
    nocapo=[]
    splittato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    return splittato

def convertifloat(mat):
    nocapo=[]
    splittato=[]
    floatato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    for x in splittato:
        for i in range(len(x)):
            x[i]=float(x[i])
        floatato.append(x)
    return floatato

docs=[
      '1DENDRO2pes.txt',
      '2DENDRO1pes.txt',
      '3DENDROinfty.txt',
      '4DENDRO3.txt',
      '5DENDRO4.txt',
      '6DENDRO5.txt',
      '7DENDRO6.txt',
      '8DENDRO7.txt'
      ]
labels=[
      '1LABELS.txt',
      '2LABELSnorm1pes.txt',
      '3LABELSnorminfty.txt',
      '4LABELSnorm3.txt',
      '5LABELSnorm4.txt',
      '6LABELSnorm5.txt',
      '7LABELSnorm6.txt',
      '8LABELSnorm7.txt'
      ]
docs = [ os.path.join('index', d) for d in  docs]
labels = [ os.path.join('index', d) for d in  labels]

minimi=20
elemento=("Nell'apparir (a 3)", "Nell'apparir (a 4)")
lab_ords_min=[]
lab_ords_max=[]
for i in range(len(docs)):
    titolo=docs[i]
    print titolo
    doc=open(titolo)
    mat=convertifloat(doc.readlines())

##    label=open(labels[ind-1])
    label=open(labels[i])
    lab1=label.readlines()
    lablist=convertistr(lab1)
    lab=[]
    for x in lablist:
        if x[0][-1]==',':
            lab.append(x[0][:-1])
        else:
            lab.append(x[0])

    doc.close()
    label.close()

    Z=np.asarray(mat)

    els_ord_min,indici,lab_ord_min=ordina_minmax(Z,verso=False,singoli=False,eti=lab)
    els_ord_max,indici,lab_ord_max=ordina_minmax(Z,verso=True,singoli=False,eti=lab)

    minimo=els_ord_min[0]
    massimo=np.max(Z)
    print minimo,massimo
    print "MEDIA", np.average(np.asarray(els_ord_min))
    print "MEDIANA", np.median(Z)
