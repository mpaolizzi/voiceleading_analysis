import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import pdist
from gerarchia import *
import os

def convertistr(mat):
    nocapo=[]
    splittato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    return splittato

def convertifloat(mat):
    nocapo=[]
    splittato=[]
    floatato=[]
    for x in mat:
        if x[-1]=='\n':
            nocapo.append(x[:-1])
        else:
            nocapo.append(x)
    for x in nocapo:
        splittato.append(x.split(', '))
    for x in splittato:
        for i in range(len(x)):
            x[i]=float(x[i])
        floatato.append(x)
    return floatato

docs=[
      '1DENDRO2pes.txt',
      '2DENDRO1pes.txt',
      '3DENDROinfty.txt',
      '4DENDRO3.txt',
      '5DENDRO4.txt',
      '6DENDRO5.txt',
      '7DENDRO6.txt',
      '8DENDRO7.txt',
      '9DENDRO8.txt',
      '99DENDRO9.txt'
      ]
docs = [ os.path.join('index', d) for d in  docs]
labels=[
      '1LABELS.txt',
      '2LABELSnorm1pes.txt',
      '3LABELSnorminfty.txt',
      '4LABELSnorm3.txt',
      '5LABELSnorm4.txt',
      '6LABELSnorm5.txt',
      '7LABELSnorm6.txt',
      '8LABELSnorm7.txt',
      '1LABELS.txt',
      '1LABELS.txt'
      ]
labels = [ os.path.join('index', d) for d in  labels]

for i in range(len(docs)):
    print 'Digitare', i+1, 'per', docs[i][:-4]
print(' ')
ind=int(raw_input('Quale documento vuoi usare? '))

doc=open(docs[ind-1])
    titolo=docs[i]
    print titolo
    doc=open(titolo)
    mat=convertifloat(doc.readlines())

label=open(labels[ind-1])
    label=open(labels[i])
    lab1=label.readlines()
    lablist=convertistr(lab1)
    lab=[]
    for x in lablist:
        if x[0][-1]==',':
            lab.append(x[0][:-1])
        else:
            lab.append(x[0])

    doc.close()
    label.close()

    Z=np.asarray(mat)

   
    methods=['complete','single','average','weighted']#,'centroid','median','ward']

    for x in methods:
        ZZ=linkage(pdist(Z),method=x,optimal_ordering=True)

        plt.figure(figsize=(20,10))
##        plt.title('Polifonia sacra - Norma '+docs[ind-1][7:-4]+', '+x)
        plt.title('Polifonia sacra - Norma '+titolo[7:-4]+', '+x)
        plt.xlabel('Opere')
        plt.ylabel('Distance')
        dendrogram(ZZ,leaf_rotation=90., leaf_font_size=10., labels=lab)
    plt.show()
